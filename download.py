import pandas as pd
import requests
import os
from tqdm import tqdm
import os.path
from urllib.parse import urlparse

def download(url, pathname):
    """
    Downloads a file given an URL and puts it in the folder `pathname`
    """
    # if path doesn't exist, make that path dir
    if not os.path.isdir(pathname):
        os.makedirs(pathname)

    url_parser = urlparse(url)

    # get the file name
    filename = url_parser.path.split("/")[-1]
    ext_name = url_parser.query.split('&')[-1].split("=")[-1]
    filename = filename + '.' + ext_name
    filename = os.path.join(pathname, filename)
    if os.path.exists(filename):
        return

    # download the body of response by chunk, not immediately
    response = requests.get(url, stream=True)
    # get the total file size
    file_size = int(response.headers.get("Content-Length", 0))

    # progress bar, changing the unit to bytes instead of iteration (default by tqdm)
    progress = tqdm(response.iter_content(1024), f"Downloading {filename}", total=file_size, unit="B", unit_scale=True, unit_divisor=1024)
    with open(filename, "wb") as f:
        for data in progress.iterable:
            # write data read to the file
            f.write(data)
            # update the progress bar manually
            progress.update(len(data))

if __name__ == '__main__':
    csv_data = pd.read_csv('./test/images.csv')
    thumbnail_list = csv_data['Thumbnail'].values

    download_path = './download'
    # if path doesn't exist, make that path dir
    if not os.path.isdir(download_path):
        os.makedirs(download_path)

    for i in range(len(thumbnail_list)):
        download(thumbnail_list[i], download_path)

    print("---- End ----");