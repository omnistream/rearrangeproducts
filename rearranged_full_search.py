import os
import glob
import time
import numpy as np
import pandas as pd
import math
from height_decrease import reduce_height
from save_html import save_html_obj
import argparse

# if sum of segment width is grater then shelf max width,
# the segment should not be set in shelf smaller than 0.2 * width_max
g_MIN_RATE = 0.2
# when block set in shelves, loop time limitation
g_TIME_LIMIT = 200
# segment position variation summary threshold
g_PERCENT_THRESHOLD = 10

g_WASTED_WIDTH_THRESHOLD = 50
g_WASTED_HEIGHT_THRESHOLD = 50


def get_max_width(shelf_index, width_ex, height, facings):
    """
    - Params:
        # shelf_index in csv
        # width_ex = width + margin * 2
        # height in csv
        # facings in csv
    - Output
        #width_max, height_sum,
    - Description
        # determine max of widths and sum of heights on shelves
    """
    width_list = []
    height_list = []
    for i, shelf_id in enumerate(shelf_index):
        while len(width_list) < shelf_id:
            width_list.append(0)
            height_list.append(0)
        width_list[shelf_id - 1] += width_ex[i] * facings[i]
        if height_list[shelf_id - 1] < height[i]:
            height_list[shelf_id - 1] = height[i]
    width_list_sorted = sorted(width_list, reverse=True)
    return width_list_sorted[0], sum(height_list)


def get_maxh_sumw(product_ids, width_ex, height, facings):
    """
    - Params:
        # ids : product id in csv(line number in csv)
        # width_ex: width + margin * 2
        # height in csv
        # facings in csv
    - Output
        #width sum, height sum of shelves,
    - Description
        # determine width and height when product in ids is set on same shelf
        # because too simple function, we used short variable names
    """
    ws = 0
    mh = 0
    for i in product_ids:
        w = width_ex[i] * facings[i]
        h = height[i]
        ws += w
        if mh < h:
            mh = h
    return ws, mh


def get_segment_list(segment, segment_priority):
    """
    - Params:
        # segment in csv
        # segment_priority in csv
    - Output
        #segment_list [segment_name, [product_id]]
    - Description
        # determine list of all segments in shelves
        # each segment contains product ids
    """
    segment_dict = dict()
    segment_priority_array = []
    for i, seg in enumerate(segment):
        if seg not in segment_dict:
            segment_dict[seg] = []
            segment_priority_array.append(segment_priority[i])
        # variable i means product_id
        segment_dict[seg].append(i)

    # [segment name , [product_id], segment_priority]
    segment_list = [[seg, segment_dict[seg], segment_priority_array[i]] for i, seg in enumerate(segment_dict)]
    # segment_list = sorted(segment_list, key=lambda k: k[2], reverse=False)
    segment_list = [[seg, ids] for seg, ids, _ in segment_list]

    return segment_list


def group_blockitem_segment(segment_list, block, width_ex, height, facings, block_sort_order, shelf_type_list,
                            variant_list, shelf_type, variant):
    """
    - Params:
      # segment_list [segment_name, [product_id]]
      # block in csv
      # width_ex in csv
      # height in csv
      # facings in csv
      # block_sort_order in csv
    - Output
     # segment_list = [segment_name, [product_id], [block_list_seg], max_block_widths, sum_block_widths]
        - block_widths = [block_width] ie.  width list of blocks in segment
        - max_block_widths is width of block width max width in segment
        - sum_block_widths is all width of this segment
        - block_list_seg = [block_name, sum_width , max_product_width, [product_id], shelf_type, variant]
            - sum_width is width of block(sum of width on product in this block)
        - other variables are all clear
    - Description
     # group all products in segments by block
     # calculate width and height of each block
    """
    for seg_info in segment_list:
        seg, product_ids = seg_info
        block_dic_seg = dict()
        for product_id in product_ids:
            b = block[product_id]
            if b not in block_dic_seg:
                block_dic_seg[b] = []
            block_dic_seg[b].append([product_id, block_sort_order[product_id]])
        # pack blocks in segment and sort by block_sort_order
        for b in block_dic_seg:
            block_dic_seg[b] = sorted(block_dic_seg[b], key=lambda k: k[1], reverse=False)

        # [block_name, sum(width of products in block) , max(height of product in block), [product_id], shelf_type, variant]
        block_list_seg = [[b, 0, 0, [kk[0] for kk in block_dic_seg[b]], 0] for b in block_dic_seg]
        for bls in block_list_seg:
            _, _, _, product_ids, _ = bls
            ws, mh = get_maxh_sumw(product_ids, width_ex, height, facings)
            bls[1] = ws
            bls[2] = mh
            st = shelf_type[product_ids[0]]
            st_id = shelf_type_list.index(st)
            v = variant[product_ids[0]]
            v_idx = variant_list.index(v)
            bls[4] = st_id * 100 + v_idx

        # for separate variant
        block_list_seg = sorted(block_list_seg, key=lambda k: k[2], reverse=True)
        block_widths = [w for _, w, _, _, _ in block_list_seg]
        seg_info.append(block_list_seg)
        seg_info.append(max(block_widths))
        seg_info.append(sum(block_widths))

    # segment_list = sorted(segment_list, key=lambda k: k[3], reverse=True)
    return segment_list


def increase_case_full_step(start_width, iter, block_ids, block_width, width_max, width_min):
    """
    - Params:
      # start_width : in this proc, must be start_width + w_sum <= width_max
      # iter: index array selected in block_ids
      # block_ids : block_ids for select
      # block_width : width of block_width
      # width_max : maximum of shelf
    - Output
     # w_sum, result of width after select
    - Description
     # no limitation of iter length
    """
    w_sum = 0
    b_Up = False
    while True:
        w_sum = start_width
        if b_Up:
            id_len = len(iter)
            num_block = len(block_ids)
            if id_len > 0:
                inc_pos = -1
                i = id_len - 1
                buf = num_block - id_len
                while i >= 0:
                    if iter[i] != buf + i:
                        inc_pos = i
                        break
                    i -= 1
                if inc_pos == -1:
                    # cannot increase
                    return -1

                inc_id = iter[inc_pos]
                iter[inc_pos:] = []
                inc_id += 1
                w_sum += sum([block_width[i] for i in iter])
            else:
                inc_id = 0
        else:
            if len(iter) > 0:
                w_sum += sum([block_width[i] for i in iter])
                inc_id = max(iter) + 1
            else:
                inc_id = 0

        add_cnt = 0
        is_added = True
        while is_added:
            is_added = False
            for j in range(inc_id, len(block_ids)):
                ww = block_width[j]
                if ww + w_sum <= width_max:
                    iter.append(j)
                    w_sum += ww
                    inc_id = j + 1
                    add_cnt += 1
                    is_added = True
                    break
            if w_sum >= width_min:
                break
        if len(iter) == 0:
            # cannot increase
            return -2

        b_Up = True
        if add_cnt == 0:
            b_Up = True
            continue
        if w_sum >= width_min:
            break

    return w_sum


def select_n_block(start_width, n_block, block_width, width_max):
    """
    - Params:
      # start_width : in this proc, must be start_width + w_sum <= width_max
      # n_block: index array selected in block_ids
      # block_width : width of block_width
      # width_max : maximum of shelf
    - Output
     # iter, result of width after select
    - Description
     # select n_block blocks in block_width to satisfy start_width + sum <= width_max
    """
    w_sum = start_width
    iter = []
    i = 0
    it0 = 0
    while len(iter) < n_block:
        # if iter[i] >= 0:
        #     it0 = iter[i] + 1
        # else:
        #     it0 = 0 if i == 0 else iter[i-1] + 1
        b_added = False
        for it in range(it0, len(block_width)):
            ww = block_width[it]
            if ww + w_sum <= width_max:
                iter.append(it)
                it0 = it + 1
                w_sum += ww
                b_added = True
                break
        if b_added:
            i += 1
        else:
            return None

    return iter


def increase_case_one_step(start_width, iter, block_ids, block_width, width_max):
    """
    - Params:
      # start_width : in this proc, must be start_width + w_sum <= width_max
      # iter: index array selected in block_ids
      # block_ids : block_ids for select
      # block_width : width of block_width
      # width_max : maximum of shelf
    - Output
     # w_sum, result of width after select
    - Description
     # not increase iter length
     # if it is impossible to go next case, increase length of iter only by 1
    """
    id_len = len(iter)
    num_block = len(block_ids)
    w_sum = start_width
    if id_len > 0:
        inc_pos = -1
        i = id_len - 1
        buf = num_block - id_len
        while i >= 0:
            if iter[i] != buf + i:
                inc_pos = i
                break
            i -= 1
        if inc_pos == -1:
            id_len += 1
            its = select_n_block(start_width, id_len, block_width, width_max)
            if its is None:
                return -3

            iter[:] = its
            w_sum += sum([block_width[i] for i in iter])
            return w_sum
        else:
            inc_id = iter[inc_pos]
            iter[inc_pos:] = []
            inc_id += 1
            w_sum += sum([block_width[i] for i in iter])
    else:
        id_len = 1
        inc_id = 0

    while len(iter) < id_len:
        b_added = False
        for j in range(inc_id, len(block_ids)):
            ww = block_width[j]
            if ww + w_sum <= width_max:
                iter.append(j)
                w_sum += ww
                inc_id = j + 1
                b_added = True
                break
        if not b_added:
            return -3
        if len(iter) == 0:
            # cannot increase
            return -2

    return w_sum


def set_next_one_shelf_case(shelf, block_array, segment_list, need_segment_ids, width_max):
    """
     - Params:
       # shelf :
       # block_array : go to function put_block2shelf(...) and see comment of variable definition block_array
       # need_segment_ids :
       # extract_segment_ids :
       # width_max : maximum width of one shelf
     - Output
      # shelf,
      # rest_block_ids,
      # ret_status,
      #      9: need_segment is invalid
      #      8: group_need_segment_iter is full
      #      7: mixed_block_iter is full
      #      6: after group_need_segment_iter is set, can't put into mixed_block_iter
      #      0: ok
     - Description
      # goto first case
      #
     """
    w_sum, search_pos_info, proc_block_ids, segment_ids, width_min, _, _ = shelf

    if len(search_pos_info) == 0:
        need_block_ids = [[] for i in range(len(need_segment_ids))]
        else_block_ids = [i for i in proc_block_ids if block_array[i][0] not in need_segment_ids]
        for i in proc_block_ids:
            sid = block_array[i][0]
            if sid in need_segment_ids:
                k = need_segment_ids.index(sid)
                need_block_ids[k].append(i)

        need_iter = [[] for g in need_block_ids]

        # index in need_group for next iter, index in in mixed_group, group_width, else_width, search_ids, ...
        search_pos_info[:] = [need_iter, [], 0, 0, need_block_ids, else_block_ids]

    need_iter, else_iter, need_width, else_width, need_block_ids, else_block_ids = search_pos_info

    need_width_min = [min([block_array[i][2] for i in gnbi]) for gnbi in need_block_ids]
    need_width_0 = need_width_min[0] if len(need_width_min) > 0 else 0
    min_need_width_sum = sum(need_width_min[1:])
    if min_need_width_sum + need_width_0 > width_max:
        return None, None, 9  # invalid need_segment_ids, need for reconstructing

    need_width_array = [[block_array[i][2] for i in gb] for gb in need_block_ids]
    else_width_array = [block_array[id][2] for id in else_block_ids]

    while (w_sum < width_min) or (width_max < w_sum):
        if len(need_block_ids) > 0:
            while (w_sum < width_min) or (width_max < w_sum):
                n_need = len(need_iter)
                """ 
                # for check if next need iter is empty 
                """
                len_iter = [1 for g_id in range(1, n_need) if len(need_iter[g_id]) == 0]

                else_width = sum([else_width_array[i] for i in else_iter])

                g_id = n_need - 1

                if len(len_iter) > 0:
                    while g_id > 0:
                        need_w_n1 = sum([need_width_min[i] for i in range(0, g_id)])
                        need_w_n2 = sum([need_width_array[i][k] for i in range(g_id + 1, n_need) for k in need_iter[i]])
                        start_w = else_width + need_w_n1 + need_w_n2

                        sid = need_segment_ids[g_id]
                        segment_width = segment_list[sid][4]
                        cut_wid = g_MIN_RATE * width_max

                        one_group_iter = need_iter[g_id]
                        group_width_i = need_width_array[g_id]
                        one_group_need_ids = need_block_ids[g_id]
                        while True:
                            w_sum = increase_case_one_step(start_w, one_group_iter, one_group_need_ids, group_width_i,
                                                           width_max)
                            if w_sum > 0:
                                cur_width = w_sum - start_w
                                rest_width = sum(group_width_i) - cur_width
                                prev_width = segment_width - cur_width - rest_width
                                if segment_width > width_max and prev_width > 0 and rest_width > 0 and start_w > 0:
                                    continue
                                if rest_width > 0 and (cur_width < cut_wid or rest_width < cut_wid):
                                    continue
                            break
                        if w_sum < 0:
                            g_id += 1
                            if g_id == n_need:
                                return None, None, 8
                        else:
                            g_id -= 1

                group_width_n = sum([need_width_array[g_id][i] for g_id in range(1, n_need) for i in need_iter[g_id]])
                start_w = else_width + group_width_n

                sid = need_segment_ids[0]
                segment_width = segment_list[sid][4]
                cut_wid = g_MIN_RATE * width_max

                one_group_iter = need_iter[0]
                group_width_i = need_width_array[0]
                one_group_need_ids = need_block_ids[0]
                group_width_0 = sum(group_width_i)
                w_sum = 0
                while True:
                    w_sum = increase_case_full_step(start_w, one_group_iter, one_group_need_ids, group_width_i,
                                                    width_max, width_min)
                    if w_sum > 0:
                        cur_width = w_sum - start_w
                        rest_width = group_width_0 - cur_width
                        prev_width = segment_width - cur_width - rest_width
                        if segment_width > width_max and prev_width > 0 and rest_width > 0 and start_w > 0:
                            continue
                        if rest_width > 0 and (cur_width < cut_wid and rest_width < cut_wid):
                            continue
                    break

                if w_sum > 0:
                    continue

                need_width_0 = group_width_0 if segment_width > width_max else need_width_0

                inc_index = -1
                for g_id in range(1, n_need):
                    group_width_n1 = sum([need_width_min[i] for i in range(1, g_id)])
                    group_width_n2 = sum(
                        [need_width_array[i][k] for i in range(g_id + 1, n_need) for k in need_iter[i]])
                    start_w = else_width + need_width_0 + group_width_n1 + group_width_n2

                    sid = need_segment_ids[g_id]
                    segment_width = segment_list[sid][4]
                    cut_wid = g_MIN_RATE * width_max

                    one_group_iter = need_iter[g_id]
                    group_width_i = need_width_array[g_id]
                    one_group_need_ids = need_block_ids[g_id]
                    w_sum = 0
                    while True:
                        w_sum = increase_case_one_step(start_w, one_group_iter, one_group_need_ids, group_width_i,
                                                       width_max)
                        if w_sum > 0:
                            cur_width = w_sum - start_w
                            rest_width = sum(group_width_i) - cur_width
                            prev_width = segment_width - cur_width - rest_width
                            if segment_width > width_max and prev_width > 0 and rest_width > 0 and start_w > 0:
                                continue
                            if rest_width > 0 and (cur_width < cut_wid or rest_width < cut_wid):
                                continue
                        break

                if w_sum > 0:
                    inc_index = g_id
                    break

                if inc_index >= 0:
                    for i in range(0, inc_index):
                        need_iter[i][:] = []
                        w_sum = 1
                    continue
                else:
                    break

            if (w_sum < width_min) or (width_max < w_sum):
                else_width_array = [block_array[i][2] for i in else_block_ids]
                start_w = need_width_0 + min_need_width_sum

                w_sum = 0
                while True:
                    w_sum = increase_case_one_step(start_w, else_iter, else_block_ids, else_width_array, width_max)
                    if w_sum > 0:
                        selected_bids = [else_block_ids[i] for i in else_iter]
                        selected_sids = sorted(list(set([block_array[bid][0] for bid in selected_bids])))
                        sum_array = [sum([block_array[bid][2] for bid in selected_bids if block_array[bid][0] == sid])
                                     for sid in selected_sids]

                        cut_wid = g_MIN_RATE * width_max
                        segment_widths = [segment_list[sid][4] for sid in selected_sids]

                        unselected_bids = sorted(list(set(else_block_ids) - set(selected_bids)))
                        sum_array1 = [
                            sum([block_array[bid][2] for bid in unselected_bids if block_array[bid][0] == sid]) for sid
                            in selected_sids]

                        b_sel_ok = True
                        for i in range(len(sum_array)):
                            www = sum_array[i]
                            www1 = sum_array1[i]
                            if www1 > 0 and (www < cut_wid or www1 < cut_wid):
                                b_sel_ok = False
                                break
                        if not b_sel_ok:
                            continue
                    break

                if w_sum > 0:
                    for it in need_iter:
                        it[:] = []
                    w_sum = 1
                    continue
                else:
                    return None, None, 8
        else:
            w_sum = 0
            while True:
                w_sum = increase_case_full_step(0, else_iter, else_block_ids, else_width_array, width_max, width_min)
                if w_sum > 0:
                    selected_bids = [else_block_ids[i] for i in else_iter]
                    selected_sids = sorted(list(set([block_array[bid][0] for bid in selected_bids])))
                    sum_array = [sum([block_array[bid][2] for bid in selected_bids if block_array[bid][0] == sid]) for
                                 sid
                                 in selected_sids]

                    else_unselected_block_ids = sorted(list(set(else_block_ids) - set(selected_bids)))
                    sum_array1 = [
                        sum([block_array[bid][2] for bid in else_unselected_block_ids if block_array[bid][0] == sid])
                        for
                        sid in selected_sids]

                    cut_wid = g_MIN_RATE * width_max
                    segment_widths = [segment_list[sid][4] for sid in selected_sids]

                    is_ok = True
                    for i in range(len(sum_array)):
                        www = sum_array[i]
                        www1 = sum_array1[i]
                        segment_width = segment_widths[i]
                        if segment_width > width_max:
                            pds = 0
                        if www1 > 0 and (www < cut_wid or www1 < cut_wid):
                            is_ok = False
                            break
                    if not is_ok:
                        w_sum = 1
                        continue
                break

            if w_sum > 0:
                continue
            else:
                return None, None, 8

    used_block_ids1 = [need_block_ids[i][k] for i, g in enumerate(need_iter) for k in g]
    used_block_ids2 = [else_block_ids[i] for i in else_iter]

    group_width = sum([block_array[i][2] for i in used_block_ids1])
    else_width = sum([block_array[i][2] for i in used_block_ids2])
    shelf[0] = group_width + else_width
    search_pos_info[2] = group_width
    search_pos_info[3] = else_width
    used_block_ids = used_block_ids1 + used_block_ids2

    buf_set1 = set([block_array[i][0] for i in used_block_ids1])
    buf_set2 = set([block_array[i][0] for i in used_block_ids2])
    segment_ids[:] = list(buf_set1 | buf_set2)

    return shelf, used_block_ids, 0


def set_first_case(segment_list, block_array, num_shelf, width_max, all_width_sum):
    """
     - Params:
       # segment_list : infomation of blocks
       # block_array : go to function put_block2shelf(...) and see comment of variable definition block_array
       # num_shelf :
       # width_max : maximum width of one shelf
       # all_width_sum : sum of all blocks
     - Output
      # shelves_list : see also function get_current_case(..)
     - Description
      # goto first case
      #
     """
    block_array_sort_val = [block_array[i][4] for i in range(len(block_array))]
    received_block_ids = [i for i in range(len(block_array))]
    # [width, proc_block_indexes, proc_block_array, segment_ids, min_width, received_block_ids
    shelves_list = [[0, [], [], [], 0, [], 1] for i in range(num_shelf)]
    rest_width = all_width_sum
    extract_segment_ids = []
    i = 0
    # loopcnt = 0
    while (0 <= i) and (i < num_shelf):
        # loopcnt += 1
        # print(loopcnt)
        width_min = rest_width - width_max * (num_shelf - i - 1)
        width_min = 2 if width_min < 2 else width_min

        min_shv_val = min(block_array_sort_val[i] for i in received_block_ids)
        proc_block_ids = np.where(np.array(block_array_sort_val) == min_shv_val)[0]

        proc_block_ids = list(set(proc_block_ids) & set(received_block_ids))

        shelf = shelves_list[i]

        prev_segment_ids = shelves_list[i - 1][3] if i > 0 else []
        rest_segment_ids = [block_array[k][0] for k in proc_block_ids]

        need_segment_ids = list(set(rest_segment_ids) & set(prev_segment_ids))

        shelf[2] = proc_block_ids
        shelf[4] = width_min
        shelf[5] = received_block_ids
        shelf, used_block_ids, ret_status = set_next_one_shelf_case(shelf, block_array, segment_list, need_segment_ids,
                                                                    width_max)
        extract_segment_ids[:] = []
        if ret_status == 0:
            received_block_ids = list(set(received_block_ids) - set(used_block_ids))
            rest_width -= shelf[0]
            i += 1
        elif ret_status == 8 or ret_status == 9:
            shelves_list[i] = [0, [], [], [], 0, []]
            i -= 1
            prev_shelf = shelves_list[i]
            rest_width += prev_shelf[0]
            prev_shelf[0] = 0
            received_block_ids = prev_shelf[5]

    return shelves_list


def set_next_case(block_array, shelves_list, segment_list, width_max, all_width_sum, best_shelves_list):
    """
     - Params:
       # block_array : go to function put_block2shelf(...) and see comment of variable definition block_array
       # shelves_list : see also comment in function get_current_case
       # width_max : maximum width of one shelf
       # all_width_sum : sum of all blocks
     - Output
      # change_shelf_idx
     - Description
      # goto next case, if change_shelf_idx < 0: end
      #
     """
    num_shelves = len(shelves_list)

    extract_segment_ids = []
    i = num_shelves - 2
    while i >= 0:
        shelf = shelves_list[i]
        proc_block_ids = shelf[2]

        prev_segment_ids = shelves_list[i - 1][3] if i > 0 else []
        rest_segment_ids = [block_array[k][0] for k in proc_block_ids]

        need_segment_ids = list(set(rest_segment_ids) & set(prev_segment_ids))

        shelf[0] = 0
        if shelf[6] == 0:
            shelf[1][:] = []
            shelf[6] = 1

        shelf, used_block_ids, ret_status = set_next_one_shelf_case(shelf, block_array, segment_list, need_segment_ids,
                                                                    width_max)
        if ret_status == 0:
            break
        elif ret_status == 8 or ret_status == 9:
            i -= 1

    change_shelf_idx = i
    if i >= 0:
        block_array_sort_val = [block_array[i][4] for i in range(len(block_array))]
        received_block_ids = shelves_list[i][5]
        received_block_ids = list(set(received_block_ids) - set(used_block_ids))

        num_shelf = len(shelves_list)

        rest_width = all_width_sum - sum([shelves_list[k][0] for k in range(i + 1)])

        extract_segment_ids[:] = []
        for k in range(i + 1, num_shelf):
            shelves_list[k][:] = [0, [], [], [], 0, [], 1]

        t0 = time.time()
        loop_cnt = 0
        i += 1
        while (i >= 1) and (i < num_shelf):
            loop_cnt += 1
            min_width = rest_width - width_max * (num_shelf - i - 1)
            min_width = 2 if min_width < 2 else min_width

            min_shv_val = min(block_array_sort_val[i] for i in received_block_ids)
            proc_block_ids = np.where(np.array(block_array_sort_val) == min_shv_val)[0]
            proc_block_ids = list(set(proc_block_ids) & set(received_block_ids))

            shelf = shelves_list[i]

            prev_segment_ids = shelves_list[i - 1][3] if i > 0 else []
            rest_segment_ids = [block_array[k][0] for k in proc_block_ids]

            need_segment_ids = list(set(rest_segment_ids) & set(prev_segment_ids))

            shelf[0] = 0
            shelf[4] = min_width
            shelf[2] = proc_block_ids
            shelf[5] = received_block_ids
            if shelf[6] == 0:
                shelf[1][:] = []
                shelf[6] = 1

            # if proc_block_ids == best_shelves_list[i][2] and received_block_ids == best_shelves_list[i][5]:
            #     copy_shelves_list(best_shelves_list, shelves_list, i)
            #     break

            shelf, used_block_ids, ret_status = set_next_one_shelf_case(shelf, block_array, segment_list,
                                                                        need_segment_ids, width_max)

            if ret_status == 0:
                # if i == 2:
                #     need_iter = shelf[1][0]
                #     for nit in need_iter:
                #         if len(nit) == 0:
                #             ppp = 0
                received_block_ids = list(set(received_block_ids) - set(used_block_ids))
                rest_width -= shelf[0]
                i += 1
            elif ret_status == 8 or ret_status == 9:
                shelves_list[i][:] = [0, [], [], [], 0, [], 1]
                i -= 1
                prev_shelf = shelves_list[i]
                rest_width += prev_shelf[0]
                received_block_ids = prev_shelf[5]

            if time.time() - t0 > g_TIME_LIMIT:
                return 0

        change_shelf_idx = i

    return change_shelf_idx


def get_shelves_height(shelves_list, block_array):
    """
     - Params:
       # shelves_list: see also comment in function get_current_case
       # block_array : go to function put_block2shelf(...) and see comment of variable definition block_array
     - Output
      # height_sum
     - Description
      # calculate height of shelves
      #
     """
    block_len = 0
    height_sum = 0
    for shelf in shelves_list:
        _, search_pos_info, proc_block_ids, _, _, _, _ = shelf
        if len(search_pos_info) == 0:
            return 7777777777777, 0
        need_iter, else_iter, _, _, need_block_ids, else_block_ids = search_pos_info

        used_ids1 = [need_block_ids[i][k] for i, ids in enumerate(need_iter) for k in ids]
        used_ids2 = [else_block_ids[i] for i in else_iter]
        used_ids = used_ids1 + used_ids2
        heights = [block_array[i][3] for i in used_ids]

        height_max = max(heights)

        height_sum += height_max
        block_len += len(heights)
    return height_sum, block_len


def copy_shelves_list(from_shelves, to_shelves, start_shid):
    """
     - Params:
       # from_shelves : see also comment in function get_current_case
       # to_shelves : see also comment in function get_current_case
     - Output
      # ,
     - Description
      # copy from form_shelf into to_shelves so that best_case is saved
      #
     """
    num_shelves = len(from_shelves)
    for i in range(start_shid, num_shelves):
        from_shelf = from_shelves[i]
        to_shelf = to_shelves[i]

        to_shelf[0] = from_shelf[0]
        to_shelf[4] = from_shelf[4]

        to_shelf[1] = [[], [], 0, 0, [], []]
        to_shelf1 = to_shelf[1]
        from_shelf1 = from_shelf[1]

        f0 = []
        for f in from_shelf1[0]:
            f0.append([])
            f0[-1][:] = f

        f4 = []
        for f in from_shelf1[4]:
            f4.append([])
            f4[-1][:] = f

        to_shelf1[0] = f0
        to_shelf1[1][:] = from_shelf1[1]
        to_shelf1[2] = from_shelf1[2]
        to_shelf1[3] = from_shelf1[3]
        to_shelf1[4][:] = f4
        to_shelf1[5][:] = from_shelf1[5]

        to_shelf[2][:] = from_shelf[2][:]
        to_shelf[3][:] = from_shelf[3][:]
        to_shelf[5][:] = from_shelf[5][:]
        to_shelf[6] = from_shelf[6]


def get_current_case(segment_list, block_array, shelf_index, width_max, all_width_sum):
    """
     - Params:
       # segment_list : for detail, see also function group_blockitem_segment(....)
       # block_array : go to function put_block2shelf(...) and see comment of variable definition block_array
          [segment_id, block_id, block_width, block_height, variants]
       # shelf_index : list in csv
       # width_max : maximum width of one shelf
       # all_width_sum : width sum of all blocks
     - Output
      # shelves_list: information about blocks sited on shelves
         [width_sum, search_pos_info, [proc_block_id], [used_block_id], width_min, [received_block_id]]
             - width_sum: width of this shelf
             - [proc_block_id] : processing blocks for optimization. we can replace each other in this list
             - [used_block_id] : current selected blocks
             - width_min: min width of current shelf.
             - [received_block_id] : [all block_id] \ [pre processed block_id]
                * It is same with proc_block_id in most of case
                * but, It differ from proc_block_id in case that varients value differs
                * when increaseing shelf id, length of this list is decreased
            - search_pos_info : [need_iter, else_iter, need_width, else_width, need_block_ids, else_block_ids]
                * if in previous shelf contains segments with id s1, s2, s3 and proc_block_id contains segment with s2 and s3,
                    current shelf should contain segment of s2, s3
                - need_block_ids = [need_block_ids2, [need_block_ids3]]
                    - need_block_ids2 = [block_id] is blockids contains s2
                    - need_block_ids3 = [block_id] is blockids contains s3
                - else_block_ids = [proc_block_id] \ [need_block_ids]
                - else_width = sum of block in else_block_ids
                - need_width = sum of block in need_block_ids
                - need_iter = [[need_iter2], [need_iter3]]
                    - need_iter2 : current selection in need_block_ids2
                    - need_iter3 : current selection in need_block_ids3
                - else_iter : current selection in else_block_ids
     - Description
      # set current case as start point for searching
      #
     """
    shelf_index_dict = list(set(shelf_index))
    shelf_index_dict = sorted(shelf_index_dict)
    num_shelf = len(shelf_index_dict)

    # [[block_ids]]
    shelves_list_buf = [[] for i in range(num_shelf)]
    for bb_id, b in enumerate(block_array):
        segment_id, product_id, w, h, order = b
        seg_name, _, blocks, _, _ = segment_list[segment_id]
        block = blocks[product_id]
        block_name, w, h, product_ids, _ = block
        shids = list(set([shelf_index[i] for i in product_ids]))

        if len(shids) > 1:
            print('Violate segment %s block_name = %s' % (seg_name, block_name) + ' '.join([str(i) for i in shids]))
            return None

        shid = shelf_index_dict.index(shids[0])
        shelf_buf = shelves_list_buf[shid]
        shelf_buf.append(bb_id)

    rest_width = all_width_sum
    received_block_ids = [i for i in range(len(block_array))]
    block_array_sort_val = [block_array[i][4] for i in range(len(block_array))]

    need_segnents = []
    #
    shelves_list = [[0, [], [], [], 0, [], 0] for i in range(num_shelf)]
    for shid, used_block_ids in enumerate(shelves_list_buf):
        shelf = shelves_list[shid]
        width_sum = sum([block_array[i][2] for i in used_block_ids])
        # from all rest width, we can calculate width_min of current shelf
        # if current shelf width shorter then this value,
        # although next shelf fill with width_max, rest products can be occured.
        width_min = rest_width - width_max * (num_shelf - shid - 1)
        width_min = 2 if width_min < 2 else width_min

        min_shv_val = min(block_array_sort_val[i] for i in received_block_ids)
        proc_block_ids = np.where(np.array(block_array_sort_val) == min_shv_val)[0]
        proc_block_ids = sorted(list(set(proc_block_ids) & set(received_block_ids)))

        prev_segment_ids = shelves_list[shid - 1][3] if shid > 0 else []
        rest_segment_ids = [block_array[k][0] for k in proc_block_ids]
        need_segment_ids = sorted(list(set(rest_segment_ids) & set(prev_segment_ids)))

        shelf[0] = width_sum
        shelf[2][:] = proc_block_ids
        shelf[3][:] = sorted(list(set([block_array[i][0] for i in used_block_ids])))
        shelf[4] = width_min
        shelf[5][:] = received_block_ids

        search_pos_info = shelf[1]

        need_block_ids = [[] for i in range(len(need_segment_ids))]
        for i in proc_block_ids:
            sid = block_array[i][0]
            if sid in need_segment_ids:
                k = need_segment_ids.index(sid)
                need_block_ids[k].append(i)
        else_block_ids = [i for i in proc_block_ids if block_array[i][0] not in need_segment_ids]

        need_iter = [[i for i, id in enumerate(nb) if id in used_block_ids] for nb in need_block_ids]
        else_used_block_ids = sorted(list(set(used_block_ids) & set(else_block_ids)))
        else_iter = [else_block_ids.index(i) for i in else_used_block_ids]

        need_width = sum([block_array[need_block_ids[i][k]][2] for i, nit in enumerate(need_iter) for k in nit])
        else_width = sum([block_array[else_block_ids[i]][2] for i in else_iter])

        search_pos_info[:] = [need_iter, else_iter, need_width, else_width, need_block_ids, else_block_ids]

        received_block_ids = sorted(list(set(received_block_ids) - set(used_block_ids)))
        rest_width -= width_sum

    return shelves_list


def get_rectangle_criterion(shelves_list, block_array, n_segment):
    """
    - Params:
      # shelves_list : for detail, see also function get_current_case(....)
      # block_array : go to function put_block2shelf(...) and see comment of variable definition block_array
      # n_segment : number of segments
    - Output
     # rectangle_criterion,
    - Description
     # put all block in segments into shelf by full searching
     #
    """
    segment_split_info = [[] for i in range(n_segment)]
    for shid, shelf in enumerate(shelves_list):
        search_pos_info = shelf[1]
        need_iter, else_iter, need_width, else_width, need_block_ids, else_block_ids = search_pos_info

        blocks1 = [block_array[else_block_ids[i]] for i in else_iter]
        blocks2 = [block_array[need_block_ids[i][k]] for i, it in enumerate(need_iter) for k in it]
        blocks = blocks1 + blocks2
        shelf_segs = list(set([s for s, _, _, _, _ in blocks]))

        widths = [0 for i in range(len(shelf_segs))]
        for s, _, w, _, _ in blocks:
            idx = shelf_segs.index(s)
            widths[idx] += w

        for i in range(len(widths)):
            w = widths[i]
            s = shelf_segs[i]
            segment_split_info[s].append([shid, w])

    rectangle_criterion = 0
    for segment_split in segment_split_info:
        n_seg = len(segment_split)
        if n_seg < 2: continue

        w_mean = 0
        w_var = 0
        for _, w in segment_split:
            w_mean += w
            w_var += w * w
        w_mean /= n_seg
        w_var = w_var / n_seg - w_mean * w_mean
        w_var = math.sqrt(w_var)

        rectangle_criterion += w_var

    return rectangle_criterion


def put_block2shelf(segment_list, shelf_index, width_max, num_shelf, height_sum):
    """
    - Params:
      # segment_list : for detail, see also function group_blockitem_segment(....)
      # width_max
      # num_shelf
      # height_sum
    - Output
     # shelves_info,
    - Description
     # put all block in segments into shelf by full searching
     #
    """

    # [segment_id, block_id, block_width, block_height, variants]
    block_array = []
    for sid, (_, _, blockes, _, _) in enumerate(segment_list):
        for bid, binfo in enumerate(blockes):
            block_array.append([sid, bid, binfo[1], binfo[2], binfo[4]])
    block_array = sorted(block_array, key=lambda k: k[2], reverse=True)
    min_block_width = block_array[-1][2]

    # for selecting block with maximum
    thres_idx = -1
    for i, b in enumerate(block_array):
        if (b[2] <= width_max - min_block_width) and (b[2] < width_max * 0.8):
            thres_idx = i
            break

    thres_idx = 1 if thres_idx < 1 else thres_idx

    block_array1 = block_array[:thres_idx]
    block_array2 = block_array[thres_idx:]
    block_array = block_array1 + sorted(block_array2, key=lambda k: k[3], reverse=True)

    block_array = sorted(block_array, key=lambda k: k[0], reverse=False)
    block_array = sorted(block_array, key=lambda k: k[4], reverse=False)

    # sum of all blocks
    all_width_sum = sum([w for _, _, w, _, _ in block_array])

    shelves_list = get_current_case(segment_list, block_array, shelf_index, width_max, all_width_sum)
    if shelves_list is None:
        shelves_list = set_first_case(segment_list, block_array, num_shelf, width_max, all_width_sum)

    best_shelves_list = [[0, [], [], [], 0, [], 0] for i in range(num_shelf)]

    shelves_height, block_cnt = get_shelves_height(shelves_list, block_array)
    rect_criterion = get_rectangle_criterion(shelves_list, block_array, len(segment_list))

    copy_shelves_list(shelves_list, best_shelves_list, 0)

    t0 = time.time()
    update_cnt = 0
    loop_cnt = 0
    shelves_height_min = shelves_height
    rect_criterion_min = rect_criterion
    # vertical_var_min = vertical_var
    # best_vertical_var = 0
    change_shelf_idx = num_shelf - 2
    while change_shelf_idx > 0 and loop_cnt < 10000 and (time.time() - t0 < g_TIME_LIMIT):
        shelves_height, block_cnt = get_shelves_height(shelves_list, block_array)
        rect_criterion = get_rectangle_criterion(shelves_list, block_array, len(segment_list))
        if (rect_criterion_min == rect_criterion and shelves_height_min > shelves_height) \
                or (rect_criterion_min > rect_criterion and shelves_height_min >= shelves_height):
            shelves_height_min = shelves_height
            rect_criterion_min = rect_criterion
            copy_shelves_list(shelves_list, best_shelves_list, 0)
            update_cnt += 1

        # print('loopcnt : %d' % (loop_cnt))
        change_shelf_idx = set_next_case(block_array, shelves_list, segment_list, width_max, all_width_sum,
                                         best_shelves_list)
        loop_cnt += 1

    shelves_info = []
    for shelf in best_shelves_list:
        w, search_pos_info, _, _, _, _, _ = shelf
        need_iter, else_iter, need_width, else_width, need_block_ids, else_block_ids = search_pos_info

        blocks1 = [block_array[else_block_ids[i]] for i in else_iter]
        blocks2 = [block_array[need_block_ids[i][k]] for i, it in enumerate(need_iter) for k in it]

        shelves_info.append([w, blocks1 + blocks2])

    for i, (w, sb_infos) in enumerate(shelves_info):
        sb_infos = sorted(sb_infos, key=lambda k: k[1], reverse=False)
        sb_infos = sorted(sb_infos, key=lambda k: k[0], reverse=False)
        shelves_info[i][1] = sb_infos

    return shelves_info


def get_height(rows, height):
    """
    - Params:
        # rowss
    - Output
        #height
    - Description
        # for pack eack segment in shelf for move it in shelf
    """
    height_sum = 0
    for shid, r in enumerate(rows):
        max_h = 0
        for id in r:
            h = height[id]
            if max_h < h:
                max_h = h

        height_sum += max_h

    return height_sum


def pack_by_segment(shelves_info):
    """
    - Params:
        # shelves_info
    - Output
        #id shelves_info
    - Description
        # for pack eack segment in shelf for move it in shelf
    """
    prev_seg_array = []
    for shid, (_, s_info) in enumerate(shelves_info):
        seg_array = []
        seg_info_dict = dict()
        for i, (sid, bid, _, _, _) in enumerate(s_info):
            if str(sid) not in seg_info_dict:
                seg_info_dict[str(sid)] = []
                seg_array.append(sid)
            seg_info_dict[str(sid)].append(i)

        commom_seg_array = list(set(prev_seg_array) & set(seg_array))
        rest_seg_array = list(set(seg_array) - set(commom_seg_array))
        seg_array = commom_seg_array + rest_seg_array

        seg_info_list = []
        for s in seg_array:
            ids = seg_info_dict[str(s)]
            w = sum([s_info[i][2] for i in ids])
            h = max([s_info[i][3] for i in ids])
            # segment id, width, height, blockids, end_left and right tag
            seg_info_list.append([s, w, h, ids, 0])

        shelves_info[shid].append(seg_info_list)

        prev_seg_array[:] = seg_array[:]
    return shelves_info


def get_segment_name_list(row_seg_info):
    seg_names_list = []
    for i, r in enumerate(row_seg_info):
        for j, s_info in enumerate(r):
            sid, startp, endp, wid, end_tag = s_info
            while len(seg_names_list) < sid + 1:
                # xxx, xxxx, xxx, []
                seg_names_list.append([0, 0, 0, []])
            # [start, end, width, shid, colid, constraint(for only left or right)]
            seg_names_list[sid][3].append([startp, endp, wid, i, j, end_tag])

    return seg_names_list


def calc_cent_pos(sna_v):
    cent_x = 0
    sum_w = 0
    var_x = 0
    for start_p, end_p, wid, rowid, colid, _ in sna_v:
        weight = 1
        c = (start_p + end_p) / 2
        # c = start_p
        cent_x += weight * c
        var_x += weight * c * c
        sum_w += weight

    cent_x /= sum_w
    var_x = var_x / sum_w - cent_x * cent_x
    return cent_x, var_x, sum_w


def update_cent_var(seg_names_all):
    var_sum = 0
    for i, sna in enumerate(seg_names_all):
        cent_x, var_x, sum_w = calc_cent_pos(sna[3])
        sna[0] = cent_x
        sna[1] = var_x
        sna[2] = sum_w
        var_sum += var_x
    return var_sum


def move_one_segment(shid, colid, end_const, seg_names_all, row_seg_info):
    """
    - Params:
      # cent_x : center of segment
      # var_x : variation of segment
      # w_sum : sum of weight of segments
      # i, j : current position of sege
      # row_seg_info
    - Output
     # min_dist_idx :
    - Description
     # determine next position of segment.
     #
    """
    cur_row_seg_info = row_seg_info[shid]
    if len(cur_row_seg_info) < 2:
        return -1

    min_var_sum = sum([seg_names_all[sid][1] for sid, _, _, _, _ in cur_row_seg_info])

    cur_row_seg_info_buf = [c[:] for c in cur_row_seg_info]

    min_dist_idx = -1
    move_list = [i for i in range(len(cur_row_seg_info))] if end_const == 0 else [0, len(cur_row_seg_info) - 1]
    for kk in range(len(move_list)):
        k = move_list[kk]
        if k == colid:
            continue

        c1 = cur_row_seg_info_buf[colid]
        c2 = cur_row_seg_info_buf[k]

        if c1[4] > 0 or c2[4] > 0:
            continue

        if c1[4] == 1 and c2[4] == 1:
            cur_row_seg_info_buf.remove(c1)
            cur_row_seg_info_buf.remove(c2)
            if k < colid:
                cur_row_seg_info_buf.insert(k, c1)
                cur_row_seg_info_buf.insert(colid, c2)
            else:
                cur_row_seg_info_buf.insert(colid, c2)
                cur_row_seg_info_buf.insert(k, c1)
        else:
            cur_row_seg_info_buf.remove(c1)
            cur_row_seg_info_buf.insert(k, c1)

        new_var_sum = 0
        sp1 = 0
        for sid, sp, ep, ww, _ in cur_row_seg_info_buf:
            loc_p = (sp + ep) / 2
            # loc_p = sp
            ep1 = sp1 + ww
            weight = 1

            n_loc_p = (sp1 + ep1) / 2
            cen_x, var_x, w_sum, sna_v = seg_names_all[sid]
            n_cenx = cen_x + (n_loc_p - loc_p) * weight / w_sum
            n_var_x = (var_x + cen_x * cen_x) + (n_loc_p * n_loc_p - loc_p * loc_p) * weight / w_sum - n_cenx * n_cenx
            new_var_sum += n_var_x

            sp1 = ep1

        if min_var_sum > new_var_sum:
            min_var_sum = new_var_sum
            min_dist_idx = k

        cur_row_seg_info_buf = [c[:] for c in cur_row_seg_info]

    return min_dist_idx


def reset_seg_pos(seg_infos):
    """
    - Params:
      # seg_infos
    - Output
     # ,
    - Description
     # reset infomation(start and end position) of segment .
     #
    """
    start_pos = 0
    for i, seg_info in enumerate(seg_infos):
        _, _, _, w, _ = seg_info
        seg_info[1] = start_pos
        end_pos = start_pos + w
        seg_info[2] = end_pos
        start_pos = end_pos


def rearrange_one_segment(sna_v, seg_names_all, row_seg_info):
    """
    - Params:
      # cent_x : center of segment
      # var_x : variation of segment.
      # w_sum : weigth sum of segment (weight each block is set by width)
      # sna_v : infomation of segment position
      # row_seg_info
    - Output
     # bupdated,
    - Description
     # move one segment in one shelf to minimize variation of segment.
     #
    """
    bupdated = False
    for kk, (sp, ep, ww, i, j, end_const) in enumerate(sna_v):
        replaced_idx = move_one_segment(i, j, end_const, seg_names_all, row_seg_info)
        if replaced_idx >= 0:
            seg_infos = row_seg_info[i]
            c1 = seg_infos[j]
            c2 = seg_infos[replaced_idx]

            if c1[4] == 0 and c2[4] == 1:
                print('replace key error (%d, %d)' % (i, j))

            if c1[4] == 1 and c2[4] == 1:
                seg_infos.remove(c1)
                seg_infos.remove(c2)
                if replaced_idx < j:
                    seg_infos.insert(replaced_idx, c1)
                    seg_infos.insert(j, c2)
                else:
                    seg_infos.insert(j, c2)
                    seg_infos.insert(replaced_idx, c1)
            else:
                seg_infos.remove(c1)
                seg_infos.insert(replaced_idx, c1)

            # nn = seg_infos[j]
            # seg_infos.remove(nn)
            # seg_infos.insert(replaced_idx, nn)

            reset_seg_pos(seg_infos)
            bupdated = True
            break

    return bupdated


def set_shelf_endtag(shelves_info, n_segment):
    """
    - Params:
      # shelves_info : total info of shelves.
    - Output
     #
    - Description
     # set end_tag about segment occupied alone in one shelf.
     # if end_tag is 1, the segment sited left or right corner of shelf
    """
    taged_segs = [[0, []] for i in range(n_segment)]
    for shid, (_, _, shelf_segs) in enumerate(shelves_info):
        for shelf_cell in shelf_segs:
            sid = shelf_cell[0]
            tag_info = taged_segs[sid]
            tag_info[1].append(shid)
            if len(shelf_segs) == 1: tag_info[0] = 100

    for shid, (_, _, shelf_segs) in enumerate(shelves_info):
        prev_align = 0  # if 1 left, if 2 right
        for colid, shelf_cell in enumerate(shelf_segs):
            sid = shelf_cell[0]
            tag_info = taged_segs[sid]
            tag, shids = tag_info
            if tag == 100:
                if len(shelf_segs) > 1:
                    if prev_align == 0:
                        # if this segment is put next shelf, right, else left
                        next_shids = [shid1 for shid1 in shids if shid1 > shid]
                        # 1: right, 2: left
                        prev_align = 2 if len(next_shids) > 0 else 1
                    else:
                        prev_align = 1 if prev_align == 2 else 2
                    tag_info[0] = prev_align
            elif tag > 0:
                prev_align = tag_info[0]

        if len(shelf_segs) == 2:
            shelf_cell0 = shelf_segs[0]
            shelf_cell1 = shelf_segs[1]

            sid0 = shelf_cell0[0]
            tag_info0 = taged_segs[sid0]
            tag0, _ = tag_info0

            sid1 = shelf_cell1[0]
            tag_info1 = taged_segs[sid1]
            tag1, _ = tag_info1

            if tag0 > 0:
                tag_info1[0] = 2 if tag0 == 1 else 1
            elif tag1 > 0:
                tag_info0[0] = 2 if tag1 == 1 else 1

    for shid, (_, _, shelf_segs) in enumerate(shelves_info):
        for colid, shelf_cell in enumerate(shelf_segs):
            sid = shelf_cell[0]
            tag_info = taged_segs[sid]
            tag, shids = tag_info
            shelf_cell[4] = tag


def reorder_shelf(shid, shelves_info):
    """
    - Params:
      # shid : current shelf id
      # shelves_info : total info of shelves.
    - Output
     #
    - Description
     # segments with end_tag move to left or right corner of shelf.
     #
    """
    shelf1 = shelves_info[shid][2]
    n_cells = len(shelf1)
    for colid in range(n_cells):
        shelf_cell = shelf1[colid]
        sid, _, _, _, end_tag = shelf_cell
        if end_tag == 1:
            if colid != 0:
                shelf1.remove(shelf_cell)
                shelf1.insert(0, shelf_cell)
        elif end_tag == 2:
            if colid != len(shelf_cell) - 1:
                shelf1.remove(shelf_cell)
                shelf1.append(shelf_cell)
    ppp = 0


def rearrange_loop(shelves_info, segment_list):
    """
    - Params:
      # shelves_info
      # segment_list : for detail, see also function group_blockitem_segment(....)
    - Output
     # rows,
    - Description
     # arrange segment on each shelf for minimizing of segment variation.
     #
    """
    set_shelf_endtag(shelves_info, len(segment_list))

    row_seg_info = []
    for shid, (_, _, r) in enumerate(shelves_info):
        start_pos = 0
        seg_poses = []
        reorder_shelf(shid, shelves_info)
        for sid, w, _, _, end_tag in r:
            end_pod = start_pos + w
            seg_poses.append([sid, start_pos, end_pod, w, end_tag])
            start_pos = end_pod

        row_seg_info.append(seg_poses)

    loop_cnt = 0
    b_updated = True
    prev_update_idx = 0
    update_idx = 0
    update_cnt = 0
    while b_updated and loop_cnt < 500:
        loop_cnt += 1
        seg_names_all = get_segment_name_list(row_seg_info)
        var_sum = update_cent_var(seg_names_all)

        if prev_update_idx == update_idx:
            update_cnt += 1
        if update_cnt > 5:
            update_idx += 1
            update_cnt = 0
        prev_update_idx = update_idx

        if update_idx >= len(seg_names_all):
            update_idx = 0
        update_list = [kk + update_idx for kk in range(len(seg_names_all))]

        b_updated = False
        for i in update_list:
            while i >= len(seg_names_all):
                i -= len(seg_names_all)
            _, _, _, sna_v = seg_names_all[i]
            if len(sna_v) < 2:
                continue

            b_updated = rearrange_one_segment(sna_v, seg_names_all, row_seg_info)
            if b_updated:
                update_idx = i
                break

    seg_names_all = get_segment_name_list(row_seg_info)

    rows = [[] for i in range(len(row_seg_info))]
    for i, rsinfo in enumerate(row_seg_info):
        shelf_info = shelves_info[i]
        for sid, _, _, _, _ in rsinfo:
            eqid = -1
            for ii, (sid1, _, _, _, _) in enumerate(shelf_info[2]):
                if sid == sid1:
                    eqid = ii
                    break
            sid1, _, _, ids, _ = shelf_info[2][eqid]
            for iii in ids:
                _, bid1, _, _, _ = shelf_info[1][iii]
                _, _, _, item_ids, _ = segment_list[sid][2][bid1]
                rows[i] += item_ids

    return rows


def rearrange_csv_data(csv_data, rows):
    """
    - Params:
      # csv_data
      # rows
    - Output
     # csv_data, old_shelf_idx, old_rowseq
    - Description
     # reset csv_data based on new row info.
     #
    """
    for c in csv_data.columns:
        c_data = csv_data[c].values

        n_data = []
        for ids in rows:
            for ii in ids:
                data = c_data[ii]
                n_data.append(data)

        csv_data[c].values[:] = n_data[:]

    shelf_index = csv_data['shelf_index'].values
    rowseq = csv_data['rowseq'].values
    facings = csv_data['facings'].values

    old_shelf_idx = [s for s in shelf_index]
    old_rowseq = [s for s in rowseq]

    cnt = 0
    shelf_id = 1
    for ids in rows:
        rowseq_id = 0
        for ii in ids:
            shelf_index[cnt] = shelf_id
            rowseq[cnt] = rowseq_id
            rowseq_id += facings[cnt]
            cnt += 1
        shelf_id += 1

    return csv_data, old_shelf_idx, old_rowseq


def build_self_row_2_id_tbl(shelf_index, rowseq, old_shelf_idx, old_rowseq):
    pos2idx = dict()
    for i in range(len(shelf_index)):
        sid = shelf_index[i]
        rid = rowseq[i]
        pos2idx[(sid, rid)] = i
    return pos2idx


def assign_shelf_type(shelf_index, shelf_type, shelf_index_set):
    shelf_type_list = []
    for st in shelf_type:
        if st not in shelf_type_list:
            shelf_type_list.append(st)

    num_shelf = len(shelf_index_set)
    shelf_type_info = [-1 for i in range(num_shelf)]
    for i, shf_id in enumerate(shelf_index):
        nshf_id = shelf_index_set.index(shf_id)
        st = shelf_type[i]
        id = shelf_type_list.index(st)
        if shelf_type_info[nshf_id] >= 0 and shelf_type_info[nshf_id] != id:
            ppp = 0
        shelf_type_info[nshf_id] = id
    return shelf_type_list, shelf_type_info


def get_variant_list(variant):
    variant_list = []
    for v in variant:
        if v not in variant_list:
            variant_list.append(v)

    return variant_list


def get_report_info(csv_data):
    """
    - Params:
      # csv_data
    - Output
     # percentage_max, total_wasted_width, total_wasted_height
    - Description
     # calculate report info from csv_data.
     #
    """
    shelf_index = csv_data['shelf_index'].values
    facings = csv_data['facings'].values
    width = csv_data['width'].values
    margin = csv_data['margin'].values
    height = csv_data['height'].values
    segment = csv_data['segment'].values
    profit = csv_data['profit'].values

    segment_list = list(set(segment))
    # shelf index array, start_point, end_point
    segment_layout_info = [[[], [], []] for i in range(len(segment_list))]

    shelves_list = list(set(shelf_index))
    shelves_list = sorted(shelves_list)
    # width, height_max, height_min
    shelves_wasted_info = [[0, 0, 777777777777] for i in range(len(shelves_list))]

    cur_x = 0
    prev_shid = 0
    n_product = len(shelf_index)
    for i in range(n_product):
        shid = shelf_index[i]

        if shid != prev_shid:
            prev_shid = shid
            cur_x = 0

        seg = segment[i]
        sid = segment_list.index(seg)
        segment_layout = segment_layout_info[sid]
        shids, starts, ends = segment_layout
        if shid not in shids:
            shids.append(shid)
            starts.append(-1)
            ends.append(0)
            seg_shid = len(shids) - 1
        else:
            seg_shid = shids.index(shid)

        w = (width[i] + 2 * margin[i]) * facings[i]
        h = height[i]

        if starts[seg_shid] < 0:
            starts[seg_shid] = cur_x
            ends[seg_shid] = cur_x
        ends[seg_shid] += w

        shid0 = shelves_list.index(shid)

        shelf_wasted = shelves_wasted_info[shid0]
        shelf_wasted[0] += w
        if shelf_wasted[1] < h:
            shelf_wasted[1] = h
        if shelf_wasted[2] > h:
            shelf_wasted[2] = h

        cur_x += w

    percentage_max = 0
    # derivation_str = ""
    for i, sli in enumerate(segment_layout_info):
        start_max = 0
        start_mean = 0
        end_mean = 0
        shids, start_points, end_points = sli
        for k, shid in enumerate(shids):
            start_p, end_p = start_points[k], end_points[k]
            start_mean += start_p
            end_mean += end_p
            if start_max < start_p: start_max = start_p
        n_segment = len(shids)
        start_mean /= n_segment
        end_mean /= n_segment

        width_seg = end_mean - start_mean
        variation = int(round(start_max - start_mean))
        segment_name = segment_list[i]
        percentage = 100 * variation // width_seg
        # if percentage > g_PERCENT_THRESHOLD:
        #     derivation_str += ("rectangle segment name:%s, split:%d, distance:%d percentage:%d\n"
        #                        % (segment_name, n_segment, variation, percentage))
        if percentage_max < percentage:
            percentage_max = percentage

    width_max = max([w for w, _, _ in shelves_wasted_info])

    total_wasted_width = 0
    total_wasted_height = 0
    # wasted_str = ''
    for w, maxh, minh in shelves_wasted_info:
        # wasted_str += 'wasted width = %d, height = %s\n' % (width_max - w, maxh - minh)
        total_wasted_width += width_max - w
        total_wasted_height += maxh - minh

    # wasted_str += 'total wasted width = %d height = %d\n' % (total_wasted_width, total_wasted_height)

    return percentage_max, total_wasted_width, total_wasted_height


def rearrange_main(csv_data):
    # item_id = csv_data['item_id'].values
    shelf_index = csv_data['shelf_index'].values
    rowseq = csv_data['rowseq'].values
    facings = csv_data['facings'].values
    width = csv_data['width'].values
    margin = csv_data['margin'].values
    height = csv_data['height'].values
    segment = csv_data['segment'].values
    block = csv_data['block'].values
    shelf_type = csv_data['shelf_type'].values
    variant = csv_data['variant'].values
    block_sort_order = csv_data['block_sort_order'].values
    segment_priority = csv_data['segment_priority'].values

    shelf_index_set = list(set(shelf_index))
    shelf_index_set = sorted(shelf_index_set, reverse=False)

    width_ex = width + margin * 2

    num_shelf = len(shelf_index_set)

    width_max, height_sum = get_max_width(shelf_index, width_ex, height, facings)

    print("Original width=%d,  height=%d" % (width_max, height_sum))

    shelf_type_list, shelf_type_info = assign_shelf_type(shelf_index, shelf_type, shelf_index_set)
    variant_list = get_variant_list(variant)

    segment_list = get_segment_list(segment, segment_priority)
    segment_list = group_blockitem_segment(segment_list, block, width_ex, height, facings, block_sort_order,
                                           shelf_type_list, variant_list, shelf_type, variant)

    shelves_info = put_block2shelf(segment_list, shelf_index, width_max, num_shelf, height_sum)
    shelves_info = pack_by_segment(shelves_info)
    rows = rearrange_loop(shelves_info, segment_list)

    new_height_sum = get_height(rows, height)

    # row_seg_info = rearrange_row_segment_block1(row_seg_info, segment_priority, block_sort_order)
    csv_data, old_shelf_idx, old_rowseq = rearrange_csv_data(csv_data, rows)
    pos2idx = build_self_row_2_id_tbl(shelf_index, rowseq, old_shelf_idx, old_rowseq)

    print("updated width=%d,  height=%d" % (width_max, new_height_sum))

    return csv_data, pos2idx, old_shelf_idx, old_rowseq, height_sum, new_height_sum


def main(f_idx, csv_file_name, tar_dec_height, is_first_deleting, summary_out_file):
    summ_str = "******* processing file name: %s, height to be decreased: %d ********" % (csv_name, tar_dec_height)
    print(summ_str)

    name_array = csv_file_name.split('.')
    # name_array.remove(name_array[-1])
    new_name = ".".join(name_array[:-1])

    csv_save_file_name = "rearranged_%s.csv" % new_name
    html_save_file_name = "rearranged_%s.html" % new_name

    csv_data = pd.read_csv(csv_file_name)

    org_profit = sum(csv_data['profit'].values)
    org_percentage_max, org_total_wasted_width, org_total_wasted_height = get_report_info(csv_data)

    if is_first_deleting:
        lost_profit, dec_height, best_case, all_h, all_prof, scores, rows, csv_data = reduce_height(tar_dec_height,
                                                                                                    csv_data)
        csv_data, pos2idx, old_shelf_idx, old_rowseq, height_sum, arranged_height_sum = rearrange_main(csv_data)
    else:
        csv_data, pos2idx, old_shelf_idx, old_rowseq, height_sum, arranged_height_sum = rearrange_main(csv_data)
        new_tar_dec_height = 0
        if tar_dec_height > 0:
            new_tar_dec_height = arranged_height_sum - (height_sum - tar_dec_height)
        lost_profit, dec_height, best_case, all_h, all_prof, scores, rows, csv_data = reduce_height(new_tar_dec_height,
                                                                                                    csv_data)
    csv_data.to_csv(csv_save_file_name, index=False, encoding='utf-8')

    save_html_obj(html_save_file_name, csv_data)

    upd_percentage_max, upd_total_wasted_width, upd_total_wasted_height = get_report_info(csv_data)
    upd_profit = sum(csv_data['profit'].values)

    summary_out_file.write('%d,,%d%s,%d,%d,%d,%d,%.01f,%.01f,%s\n' %
                           (f_idx, upd_percentage_max, '%', org_total_wasted_width, upd_total_wasted_width,
                            org_total_wasted_height, upd_total_wasted_height, org_profit, upd_profit, csv_name))

    print("- After removing items:")
    print("  height %d - %d = %d\n  profit %d - %d = %d"
          % (all_h, dec_height, all_h - dec_height, all_prof, lost_profit, all_prof - lost_profit))

    print("  removed items: ")
    for i, c in enumerate(best_case):
        if c == 0:
            continue
        cur_pos = 0
        for k in range(c):
            dh, dprof, cnt_cell = scores[i][k]
            for kk in range(cnt_cell):
                sid, rid, fv, _, _, _, _, itid = rows[i][cur_pos + kk]
                if not is_first_deleting:
                    k = pos2idx[(sid, rid)]
                    sid = old_shelf_idx[k]
                    rid = old_rowseq[k]
                print('    (item_id, shelf_index, rowseq) : (%d, %d, %d), facings = %d' % (itid, sid, rid, fv))
            cur_pos += cnt_cell

    print("- outputs: %s, %s" % (csv_save_file_name, html_save_file_name))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Rearrange shelves.")
    parser.add_argument("--csv_path", type=str,
                        default='./test',
                        help="Path to csv file.")
    parser.add_argument("--csv_name", type=str,
                        default='store_147_cat_70_gt_4R-E2_section_27.csv',
                        help="csv file name in csv_path")
    parser.add_argument("--dec_height", type=int, default=0, help="decrease height")
    parser.add_argument("--first_deleting", type=int, default=0, help="True if want delete before arranges")
    parser.add_argument("--one_test", type=int, default=0, help="set 1 for check one file")

    args = parser.parse_args()

    csv_path_name = args.csv_path
    os.chdir(csv_path_name)
    csv_name = args.csv_name

    print("\n******* processing directory name: %s ********\n" % csv_path_name)

    is_first_deleting = False if args.first_deleting == 0 else True
    is_one_test = False if args.one_test == 0 else True

    tar_dec_height = args.dec_height

    report_save_file_name = "report.csv"
    summary_out_file = open(report_save_file_name, 'w', encoding='utf-8')
    summary_out_file.write('No.,total score,percent,wasted width(original),wasted width(updated),'
                           'wasted height(original),wasted height(updated),profit(original),profit(updated),csv_name\n')
    f_idx = 1

    ts = time.time()

    if is_one_test:
        t0 = time.time()
        main(0, csv_name, tar_dec_height, is_first_deleting, summary_out_file)
        print("- time: %d\n" % (time.time() - t0))
    else:
        csv_file_prefix = "store_"
        for csv_name in glob.glob("*.csv"):
            substr = csv_name[0:len(csv_file_prefix)]
            if substr != csv_file_prefix:
                continue

            t0 = time.time()
            main(f_idx, csv_name, tar_dec_height, is_first_deleting, summary_out_file)
            f_idx += 1
            print("- time: %d\n" % (time.time() - t0))

    summary_out_file.close()

    print('The End. all_time: %d' % (time.time() - ts))
