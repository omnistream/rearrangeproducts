import pandas as pd
import numpy as np
import random


def fnum2str(fval):
    fval = round(fval, 1)

    strval = '%d' % (int((fval - int(fval)) * 10))
    fval = int(fval)
    for i in range(20):
        dev_fval = fval // 1000
        dub_fval = fval - 1000 * dev_fval
        if i == 0:
            strval = '%d.%s' % (dub_fval, strval)
        else:
            strval = '%d,%s' % (dub_fval, strval)
        if dev_fval == 0:
            break
        fval = dev_fval

    return strval


def round_6(fval):
    strv = '%.06f' % fval
    return strv


def save_html_hdr(out_file, title_num, segment_name, color_tbl):
    out_file.write('<html>\n')
    out_file.write('<head>\n')
    out_file.write('    <style>\n')
    out_file.write('    table { border: 0.2px #777 solid; align: center; table-layout: fixed}\n')
    out_file.write('    .content { width: 1800px; min-width: 1800px }\n')
    out_file.write('    div.item { position: absolute; bottom: 0; padding: 0; margin: 0; box-sizing: border-box;\n')
    out_file.write('        -moz-box-sizing: border-box;\n')
    out_file.write('        -webkit-box-sizing: border-box;\n')
    out_file.write('        border: 1px solid black}\n')
    out_file.write('    div.item:hover ul {display: block; position: fixed ; top: 0; left: 0;\n')
    out_file.write('        z-index: 999;}\n')
    out_file.write('    p.item { text-align: center; font-size: 0.8em; }\n')
    out_file.write('    td.meta ul {margin-block-start: 0; padding-inline-start: 0; list-style-type: none;}\n')
    out_file.write('    ul.description {font-size: 1.2em; display: none; background: #fff; border: 2px red solid; padding: 10px}\n')
    out_file.write('    tbody {background-color: #eee }\n')
    out_file.write('    td.meta { font-size: 0.6em ; width: 50px}\n\n')
    for i, seg in enumerate(segment_name):
        c = color_tbl[i]
        out_file.write('    .c%s { background-color: rgb(%d, %d, %d) }\n\n' % (seg, c[0], c[1], c[2]))
    out_file.write('    </style>\n')
    out_file.write('    <title>Planogram: %d Main Gondola (Section 1)</title>\n\n' % title_num)
    out_file.write('</head>\n')
    out_file.write('<body>\n\n')
    out_file.write('<table>\n')


def save_html_close(out_file):
    out_file.write('    </tbody>\n')
    out_file.write('</table>\n')
    out_file.write('</body>\n')
    out_file.write('</html>\n')


def save_html_thead(out_file, title_num, section_num, width_max, profit_sum):
    out_file.write('    <thead>\n')
    out_file.write('        <tr class=\'header\'>\n')
    out_file.write('            <td class="meta"></td>\n')
    out_file.write('            <td class="content">\n')
    out_file.write('                <h1 align="center">Bay: %d Main Gondola (Section %d)</h1>\n' % (title_num, section_num))
    out_file.write('                <p align="center">Width: %dmm. Total expected weekly profit: ₱c%s</p>\n' % (width_max, fnum2str(profit_sum)))
    out_file.write('            </td>\n')
    out_file.write('        </tr>\n')
    out_file.write('    </thead>\n')
    out_file.write('    <tbody>\n\n')


def save_html_tr(out_file, shelf_id, wid_sum, hei_max, profit_sum, wid_max, n_item):
    wid_percent = 100*wid_sum/wid_max
    out_file.write('        <tr class=\'row\' height=%dpx>\n' % (hei_max * 2 + 20))
    out_file.write('            <td class="meta">\n')
    out_file.write('                <ul>\n')
    out_file.write('                    <li>Name: shelf %d</li>\n' % shelf_id)
    out_file.write('                    <li>Height: %dmm</li>\n' % hei_max)
    out_file.write('                    <li>Exp. Profit ₱c%s</li>\n' % fnum2str(profit_sum))
    out_file.write('                    <li>Used space: %dmm (%.02f%s)</li>\n' % (wid_sum, wid_percent, '%'))
    out_file.write('                    <li>Empty space: %dmm</li>\n' % (wid_max-wid_sum))
    out_file.write('                    <li>Nb Items: %d</li>\n' % n_item)
    out_file.write('                </ul>\n')
    out_file.write('            </td>\n')
    out_file.write('            <td class="content" style=\'position:relative\'>\n\n\n')


def save_html_tr_close(out_file):
    out_file.write('            </td>\n')
    out_file.write('        </tr>\n')


def save_html_item(out_file, item_id, item_name, wid, hei, left, segment, prediction_type, sales_amount,
                   quantity, cat2_name, brand, sub_brand, block):
    out_file.write('                    <div class=\'item c%s\' id=\'%d\' style=\'height: %dpx; width: %dpx; left: %dpx\'>\n' % (segment, item_id, hei*2, wid*2, left*2))
    out_file.write('                        <p class="item">%s, %d</p>\n' % (item_name, item_id))
    out_file.write('                        <p class="item">[%s / %s]</p>\n' % (segment, prediction_type))
    out_file.write('                        <ul class="description">\n')
    out_file.write('                            <li>Name: <b>%s</b></li>\n' % item_name)
    out_file.write('                            <li>Item Id: <b>%d</b></li>\n' % item_id)
    out_file.write('                            <li>Sales: ₱%f</li>\n' % sales_amount)
    out_file.write('                            <li>Quantity: %f</li>\n' % quantity)
    out_file.write('                            <li>Width: %dmm</li>\n' % wid)
    out_file.write('                            <li>cat2: %s</li>\n' % cat2_name)
    out_file.write('                            <li>brand: %s</li>\n' % brand)
    out_file.write('                            <li>sub brand: %s</li>\n' % sub_brand)
    out_file.write('                            <li>height: %d</li>\n' % hei)
    out_file.write('                            <li>format: NA</li>\n')
    out_file.write('                            <li>block: %s</li>\n' % block)
    out_file.write('                            <li>SEGMENT: %s</li>\n' % segment)
    out_file.write('                            <li>Prediction type: %s</li>\n' % prediction_type)
    out_file.write('                            <li>Orientation: front</li>\n')
    out_file.write('                        </ul>\n')
    out_file.write('                    </div>\n\n\n\n')


def get_rand_color_tbl(n_color):

    if n_color == 0: return []

    if n_color > 1:
        diff = (230 - 20) // (n_color - 1)
    else:
        diff = (230 - 20) // n_color
    cr = []
    c0 = 20
    for i in range(n_color):
        cr.append(c0)
        c0 += diff

    cg = [0] * n_color
    cb = [0] * n_color

    cg[:] = cr[:]
    cb[:] = cr[:]

    random.seed(777)

    random.shuffle(cr)
    random.shuffle(cg)
    random.shuffle(cb)

    color_array = []
    for i in range(n_color):
        color_array.append([cr[i], cg[i], cb[i]])

    return color_array


def save_html_obj(save_file_name, csv_data):
    shelf_index = csv_data['shelf_index'].values
    rowseq = csv_data['rowseq'].values
    facings = csv_data['facings'].values
    width = csv_data['width'].values
    margin = csv_data['margin'].values
    height = csv_data['height'].values
    segment = csv_data['segment'].values
    block = csv_data['block'].values
    variant = csv_data['variant'].values
    profit = csv_data['profit'].values
    item_id = csv_data['item_id'].values
    item_name = csv_data['item_name'].values
    prediction_type = csv_data['prediction_type'].values
    sales_amount = csv_data['sales_amount'].values
    quantity = csv_data['quantity'].values
    cat2_name = csv_data['cat2_name'].values
    brand = csv_data['brand'].values
    sub_brand = csv_data['sub_brand'].values

    width_ex = width + margin * 2

    product_num = len(csv_data)

    profit_sum = 0
    shelves = []
    for i in range(product_num):
        shelf_id = shelf_index[i]
        while len(shelves) < shelf_id:
            shelves.append([0, 0, [], 0])
        shelf = shelves[shelf_id-1]
        shelf[2].append(i)
        profit_sum += profit[i]

    wid_shelves_max = 0
    hei_shelves_sum = 0
    for shelf in shelves:
        wid_sum = 0
        hei_max = 0
        profit_shelf = 0
        for i in shelf[2]:
            wid_sum += width_ex[i] * facings[i]
            hei = height[i]
            if hei_max < hei: hei_max = hei
            profit_shelf += profit[i]
        shelf[0] = wid_sum
        shelf[1] = hei_max
        shelf[3] = profit_shelf

        if wid_shelves_max < wid_sum: wid_shelves_max = wid_sum
        hei_shelves_sum += hei_max

    segment_name = []
    for seg in segment:
        if seg not in segment_name:
            segment_name.append(seg)

    color_tbl = get_rand_color_tbl(len(segment_name))

    out_file = open(save_file_name, 'w', encoding='utf-8')
    save_html_hdr(out_file, 51, segment_name, color_tbl)

    save_html_thead(out_file, 51, 1, wid_shelves_max, profit_sum)

    for shelf_id, shelf_info in enumerate(shelves):
        wid_sum, hei_max, cont, profit_shelf = shelf_info
        if len(cont) == 0: continue

        save_html_tr(out_file, shelf_id+1, wid_sum, hei_max, profit_shelf, wid_shelves_max, len(cont))
        left = 0
        for i in cont:
            for kk in range(facings[i]):
                save_html_item(out_file, item_id[i], item_name[i], width_ex[i], height[i], left, segment[i],
                               prediction_type[i], sales_amount[i],
                               quantity[i], cat2_name[i], brand[i], sub_brand[i], block[i])
                left += width[i]

        save_html_tr_close(out_file)

    save_html_close(out_file)
    out_file.close()

