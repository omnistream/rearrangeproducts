import pandas as pd


def analysis_table(item_ids, shelf_index, rowseqs, facingses, widths, heights, profits):
    rows = []
    for i, sid in enumerate(shelf_index):
        sid = sid
        item_id = item_ids[i]
        rowseq = rowseqs[i]
        facingse = facingses[i]
        width = widths[i]
        height = heights[i]
        profit = profits[i]

        while len(rows) < sid:
            rows.append([])

        rows[sid-1].append([sid, rowseq, facingse, width, height, profit, i, item_id])

    return rows


def calc_score(rows):
    scores = []
    all_h = 0
    all_profit = 0
    for i, r in enumerate(rows):
        r1 = sorted(r, key=lambda k: k[4], reverse=True)
        rows[i] = r1

        if len(r1) > 0:
            all_h += r1[0][4]

        score = []
        dh_sum = 0
        prof_sum = 0
        cell_cnt = 0
        for k in range(len(r1)):
            if k < len(r1) - 1:
                dh = r1[k][4] - r1[k+1][4]
            else:
                dh = r1[k][4]
            prof = (r1[k][5]) * r1[k][2]

            dh_sum += dh
            prof_sum += prof
            cell_cnt += 1
            all_profit += prof

            if dh > 0:
                score.append([dh_sum, prof_sum, cell_cnt])
                cell_cnt = 0

        scores.append(score)
    return scores, rows, all_h, all_profit


def calc_height_profit(cur_case, scores, cur_best_profit, tar_dec_height):
    lost_profit = 0
    dec_height = 0
    for i in range(len(scores)):
        if cur_case[i] > 0:
            pos = cur_case[i]
            sco = scores[i]
            dec_height += sco[pos-1][0]
            lost_profit += sco[pos-1][1]
            if cur_best_profit < lost_profit:
                dec_height = tar_dec_height + 20
                break

    return dec_height, lost_profit


def next_case(cur_case, score_lens, prev_addId):
    len_r = len(cur_case)
    add_idx = -1

    if prev_addId == len_r - 1:
        return -1

    if prev_addId >= 0 and prev_addId + 1 < len_r:
        cur_case[0:prev_addId+1] = [0] * (prev_addId+1)

    for i in range(prev_addId+1, len_r):
        if cur_case[i] < score_lens[i]:
            cur_case[i] += 1
            add_idx = i
            break
        else:
            cur_case[i] = 0

    if add_idx < 0:
        return -1
    return add_idx


def calc_optimize_full(scores, tar_dec_height):

    nrow = len(scores)
    if tar_dec_height <= 0:
        return [0, 0, [0] * nrow]

    best_case = [777777777777777777, 0, [0] * nrow]
    cur_case = [0] * nrow

    score_lens = [len(sco) for sco in scores]
    add_id = -1

    while True:
        dec_height, lost_profit = calc_height_profit(cur_case, scores, best_case[0], tar_dec_height)
        prev_add_id = -1

        if dec_height >= tar_dec_height:
            prev_add_id = add_id
            #lost_profit = calc_profit(cur_case, scores)
            if best_case[0] > lost_profit:
                best_case[0] = lost_profit
                best_case[1] = dec_height
                best_case[2][:] = cur_case[:]

        add_id = next_case(cur_case, score_lens, prev_add_id)
        if add_id < 0:
            break

    return best_case


def delete_cell(csv_data, best_case, rows, scores):
    remove_idx = []
    for i, c in enumerate(best_case):
        if c == 0: continue
        start_k = 0
        for k in range(c):
            dh, dprof, cnt_cell = scores[i][k]

            for kk in range(cnt_cell):
                idx = rows[i][start_k+kk][6]
                remove_idx.append(idx)
            start_k += cnt_cell

    if len(remove_idx) > 0:
        remove_idx = sorted(remove_idx, reverse=True)

    for r_idx in remove_idx:
        csv_data = csv_data[csv_data.index != r_idx]

    shelf_index = csv_data['shelf_index'].values
    rowseq = csv_data['rowseq'].values
    facings = csv_data['facings'].values

    prev_sid = 0
    for i, sid in enumerate(shelf_index):
        if prev_sid != sid:
            rid = 1
            prev_sid = sid

        rowseq[i] = rid
        rid += facings[i]

    return csv_data


def reduce_height(tar_dec_height, csv_data):
    item_ids = csv_data['item_id'].values
    shelf_index = csv_data['shelf_index'].values
    rowseq = csv_data['rowseq'].values
    facings = csv_data['facings'].values
    width = csv_data['width'].values
    margin = csv_data['margin'].values
    height = csv_data['height'].values
    profit = csv_data['profit'].values

    width_ex = width + margin * 2

    rows = analysis_table(item_ids, shelf_index, rowseq, facings, width_ex, height, profit)
    scores, rows, all_h, all_prof = calc_score(rows)
    if tar_dec_height > all_h:
        tar_dec_height = all_h
    lost_profit, dec_height, best_case = calc_optimize_full(scores, tar_dec_height)

    csv_data = delete_cell(csv_data, best_case, rows, scores)

    return lost_profit, dec_height, best_case, all_h, all_prof, scores, rows, csv_data


if __name__ == '__main__':

    tar_dec_height = 200

    csv_path_name = 'F:/sss/pogs/constrained_2021_05_21_wo_dispersion_rc4/biscuits/'
    csv_file_name = 'store_1418_cat_51_gt_4R_section_1.csv'

    csv_data = pd.read_csv(csv_path_name + csv_file_name)

    lost_profit, dec_height, best_case, all_h, all_prof, scores, rows, csv_data = reduce_height(tar_dec_height, csv_data)

    print("height %d - %d = %d\nprofit %d - %d = %d"
          % (all_h, dec_height, all_h - dec_height, all_prof, lost_profit, all_prof - lost_profit))

    for i, c in enumerate(best_case):
        if c == 0: continue
        cur_pos = 0
        for k in range(c):
            dh, dprof, cnt_cell = scores[i][k]
            for kk in range(cnt_cell):
                cell = rows[i][cur_pos+kk]
                ii = rows[i][0][6]
                print('(shelf_index, rowseq) : (%d, %d), facings = %d' % (rows[i][0][0], cell[1], cell[2]))

            cur_pos += cnt_cell
